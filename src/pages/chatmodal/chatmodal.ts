import { Component,OnInit } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ConfigApp} from "../../providers/config/config.app";

/**
 * Generated class for the ChatmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatmodal',
  templateUrl: 'chatmodal.html',
})
export class ChatmodalPage implements OnInit {

    public phpHost:any;
    imagePath: string = this.navParams.get('imagePath');

    constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public Settings: ConfigApp) {
        this.phpHost = Settings.ServerImageHost;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatmodalPage');
  }

    ngOnInit() {

    }

    closeImageModal(){
        this.viewCtrl.dismiss();
    }

}

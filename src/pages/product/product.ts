import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {GoToBasketPopupComponent} from "../../components/go-to-basket-popup/go-to-basket-popup";
import {BasketProvider} from "../../providers/api/api.basket";
import {BasketPage} from "../basket/basket";
import {ChatPage} from "../chat/chat";

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-product',
    templateUrl: 'product.html',
})
export class ProductPage {
    public product: any;
    public PrdQuan: number;
    public baskets:any[]=[];
    public is_Admin:any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public modalCtrl:ModalController,
                public basketProvider:BasketProvider) {
        this.product = navParams.get('product');
        this.PrdQuan = 1;
        this.is_Admin = localStorage.getItem('is_Admin');

        this.basketProvider.basket$.subscribe(basket => this.baskets = basket);
        console.log(this.product);
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad ProductPage');
    }
    
    async changeQuan(type) {
        type === 1 ? this.PrdQuan++ : this.PrdQuan--;
        if(this.PrdQuan < 1 ) this.PrdQuan = 1 ;
    }
    
    openModal()
    {
        console.log("Modal");
        let modal = this.modalCtrl.create(GoToBasketPopupComponent);
        modal.present();
    
        modal.onDidDismiss(data => {
            console.log("bs3 : " , this.product)
            if(data)
            {
                this.product.qnt = this.PrdQuan;
                this.basketProvider.pushToBasket(this.product);
                data == 1 ? this.navCtrl.pop() : this.navCtrl.push('BasketPage');
            }
        });
    }

    gotoChatPage() {
        this.navCtrl.push(ChatPage, { product_id: this.product.id});
    }
}

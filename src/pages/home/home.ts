import {Component, NgZone, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, Events} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {Product} from '../../providers/api/api.product';
import {ConfigApp} from "../../providers/config/config.app";
import {ProductsPage} from "../products/products";

@IonicPage()

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage  implements OnInit {
    public categoires:any[] = [];
    public Host:String;
    public MainPageproducts: any[] = this.api.MainPageproducts;
    
    constructor(public navCtrl: NavController, public zone: NgZone, public api: ApiProvider ,public config:ConfigApp, public navParams: NavParams, public events: Events) {
        this.categoires = this.api.categories;
        this.Host = this.config.ServerImageHost;
        console.log(this.categoires );
    
        this.api._MainPageproducts.subscribe(val => {
            this.zone.run(() => {
                this.MainPageproducts = val;
            });
        });


    }



    async gotoProductsPage(id)
    {
        await this.api.getSubCategories("GetSubCategoriesById",id);
        await this.api.getProducts("getProducts",this.api.subCategories[0].id);
        this.navCtrl.push('ProductsPage');
    }

    ngOnInit() {
        this.events.publish('userConnected',localStorage.getItem('is_Admin'));
    }
}

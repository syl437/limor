import {Component, NgZone} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {BasketProvider} from "../../providers/api/api.basket";
import {ConfigApp} from "../../providers/config/config.app";
import {DeletePopupComponent} from "../../components/delete-popup/delete-popup";
import {Storage} from '@ionic/storage';
/**
 * Generated class for the BasketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-basket',
    templateUrl: 'basket.html',
})
export class BasketPage {
    
    public basket: any[] = this.basketProvider.basket;
    public Host: any = this.config.ServerImageHost;
    public DeleteId:number;
    public PrdQuan: number;
    public TotalBasketPrice : number;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public basketProvider: BasketProvider,
                public zone: NgZone,
                public config: ConfigApp,
                public modalCtrl:ModalController,
                public storage:Storage,) {
        
        // this.basketProvider._basket.subscribe(val => {
        //     this.zone.run(() => {
        //         this.basket = val;
        //         console.log(this.basket)
        //     });
        // });
        this.basketProvider.basket$.subscribe(basket => this.basket = basket);
        this.basketProvider.TotalBasketPrice$.subscribe(TotalBasketPrice => this.TotalBasketPrice = TotalBasketPrice);
        this.basketProvider.calculateTotalprice();
        console.log(this.basket)
        
    }
    
    async ionViewDidLoad() {
        console.log('ionViewDidLoad BasketPage');
    }
    
    async changeQuan(i,type) {
        this.basketProvider.changeQnt(i,type);
    }
    
    async openDeleteModal(i)
    {
        this.DeleteId = i;
        let modal = this.modalCtrl.create(DeletePopupComponent);
        modal.present();
        
        modal.onDidDismiss(data => {
            if(data == 1)
            {
                this.basketProvider.deleteProduct(this.DeleteId);
            }
        });
    }
    
    async sendBasket()
    {
        this.basketProvider.sendBasket('getBasket');
        this.basketProvider.clearBasket();
        this.storage.set('basket','');
        this.navCtrl.setRoot('HomePage');
    }
    
}

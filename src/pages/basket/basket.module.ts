import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BasketPage } from './basket';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    BasketPage,
  ],
  imports: [
    IonicPageModule.forChild(BasketPage),
      ComponentsModule
  ],
})
export class BasketPageModule {}

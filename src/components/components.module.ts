import { NgModule } from '@angular/core';
import { HeaderCompComponent } from './header-comp/header-comp';
import { GoToBasketPopupComponent } from './go-to-basket-popup/go-to-basket-popup';
import {IonicPageModule} from "ionic-angular";
import { DeletePopupComponent } from './delete-popup/delete-popup';
import { ProductsComponent } from './products/products';
@NgModule({
	declarations: [HeaderCompComponent,
    GoToBasketPopupComponent,
    DeletePopupComponent,
    ProductsComponent,
    ProductsComponent],
	imports:[IonicPageModule.forChild([
            HeaderCompComponent,
            GoToBasketPopupComponent,
        DeletePopupComponent,
            ]),],
	exports: [HeaderCompComponent,
    GoToBasketPopupComponent,
    DeletePopupComponent,
    ProductsComponent,
    ProductsComponent]
})
export class ComponentsModule {}

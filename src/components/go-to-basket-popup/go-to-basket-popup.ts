import {Component, Renderer} from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the GoToBasketPopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'go-to-basket-popup',
  templateUrl: 'go-to-basket-popup.html'
})
export class GoToBasketPopupComponent {

  text: string;

  constructor(public viewCtrl: ViewController) {
  
  }
    
    onSubmit(type)
    {
        this.viewCtrl.dismiss(type);
    }

}

webpackJsonp([9],{

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_config_app__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var BasketProvider = (function () {
    function BasketProvider(loadingCtrl, storage, http, config) {
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
        this.config = config;
        //public basket:any[] = [];
        // public _basket = new Subject<any>();
        this._basket = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.basket$ = this._basket.asObservable();
        this._TotalBasketPrice = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.TotalBasketPrice$ = this._TotalBasketPrice.asObservable();
    }
    Object.defineProperty(BasketProvider.prototype, "basket", {
        get: function () { return this._basket.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BasketProvider.prototype, "TotalBasketPrice", {
        get: function () { return this._TotalBasketPrice.getValue(); },
        enumerable: true,
        configurable: true
    });
    BasketProvider.prototype.pushToBasket = function (Obj) {
        var basket = this.basket;
        var len = basket ? basket.length : 0;
        console.log("Len : ", len);
        if (len > 0)
            basket.splice(len, 0, Obj);
        else
            basket.push(Obj);
        this._basket.next(basket);
        this.storage.set('basket', JSON.stringify(basket));
        this.calculateTotalprice();
        console.log("Basket : ", basket);
    };
    BasketProvider.prototype.addLocalStorageToBasket = function (json) {
        this._basket.next(json);
    };
    BasketProvider.prototype.deleteProduct = function (i) {
        this.basket.splice(i, 1);
        this.storage.set('basket', JSON.stringify(this.basket));
        this.calculateTotalprice();
    };
    BasketProvider.prototype.changeQnt = function (i, type) {
        type === 1 ? this.basket[i].qnt++ : this.basket[i].qnt--;
        if (this.basket[i].qnt < 1)
            this.basket[i].qnt = 1;
        this.storage.set('basket', JSON.stringify(this.basket));
        this.calculateTotalprice();
    };
    BasketProvider.prototype.calculateTotalprice = function () {
        var Total = 0;
        if (this.basket) {
            this.basket.forEach(function (item) {
                Total += (item.high_price * item.qnt);
            });
            this._TotalBasketPrice.next(Total);
        }
    };
    BasketProvider.prototype.sendBasket = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var body, Basket;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("MyBasket : ", this.basket);
                        body = new FormData();
                        body.append('basket', JSON.stringify(this.basket));
                        body.append('sum', this.TotalBasketPrice.toString());
                        body.append('userid', localStorage.getItem('userid'));
                        return [4 /*yield*/, this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(function (response) { return response; })];
                    case 1:
                        Basket = _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BasketProvider.prototype.clearBasket = function () {
        this._basket.next([]);
        //let basket = this.basket;
        //basket = [];
    };
    BasketProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__config_config_app__["a" /* ConfigApp */]])
    ], BasketProvider);
    return BasketProvider;
}());

//# sourceMappingURL=api.basket.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatSevice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config_app__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ChatSevice = (function () {
    function ChatSevice(http, Settings, storage) {
        this.http = http;
        this.Settings = Settings;
        this.storage = storage;
        this.ChatArray = [];
        this._ChatArray = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.ChatArray$ = this._ChatArray.asObservable();
        this.ServerUrl = this.Settings.ServerUrl;
    }
    ;
    ChatSevice.prototype.addTitle = function (url, title, datetime, time, name, image, chatType, product_id, is_Admin, recipent) {
        var body = new FormData();
        body.append('uid', window.localStorage.userid);
        body.append('title', title);
        body.append('type', chatType);
        body.append('product_id', product_id);
        body.append('is_Admin', is_Admin);
        body.append('recipent', recipent);
        body.append('date', datetime);
        body.append('time', time);
        body.append('name', name);
        //body.append('image', image );
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("Chat : ", data); }).toPromise();
    };
    ChatSevice.prototype.getChatDetails = function (url, user_id, product_id) {
        var _this = this;
        var body = new FormData();
        body.append('user_id', user_id);
        body.append('product_id', product_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.updateChatArray(data); }).toPromise();
    };
    ChatSevice.prototype.getProductInfo = function (url, product_id) {
        var body = new FormData();
        body.append('product_id', product_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    ChatSevice.prototype.updateChatArray = function (data) {
        this.ChatArray = data;
        this._ChatArray.next({ text: this.ChatArray });
    };
    ChatSevice.prototype.pushToArray = function (data) {
        //this.ChatArray.push(data);
        this.ChatArray.splice((this.ChatArray.length), 0, data);
        this._ChatArray.next(this.ChatArray);
    };
    ChatSevice.prototype.registerPush = function (url, pushid) {
        var body = new FormData();
        //body.append('push_id', pushid.toString() );
        //body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("chat:", data); });
    };
    ChatSevice.prototype.getChatMessagesCount = function (url) {
        var body = new FormData();
        // body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    ChatSevice.prototype.getAdminMessages = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Content */])
    ], ChatSevice.prototype, "content", void 0);
    ChatSevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config_app__["a" /* ConfigApp */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], ChatSevice);
    return ChatSevice;
}());

;
//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_config_app__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var ApiProvider = (function () {
    function ApiProvider(loadingCtrl, storage, http, config, restangular) {
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
        this.config = config;
        this.restangular = restangular;
        this._categories = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.categories$ = this._categories.asObservable();
        this._subCategories = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.subCategories$ = this._subCategories.asObservable();
        this._products = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.products$ = this._products.asObservable();
        this._MainPageproducts = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.MainPageproducts$ = this._MainPageproducts.asObservable();
    }
    Object.defineProperty(ApiProvider.prototype, "categories", {
        get: function () { return this._categories.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "subCategories", {
        get: function () { return this._subCategories.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "products", {
        get: function () { return this._products.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "MainPageproducts", {
        get: function () { return this._MainPageproducts.getValue(); },
        enumerable: true,
        configurable: true
    });
    // Get all categories ;
    ApiProvider.prototype.getCategories = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, categories, err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    body.append('id', '0');
                                    return [4 /*yield*/, this.restangular.all(url).customPOST(body).toPromise()];
                                case 2:
                                    categories = _a.sent();
                                    //let categories = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                                    this._categories.next(categories);
                                    resolve(this.categories);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_1 = _a.sent();
                                    console.log(err_1);
                                    reject(err_1);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    // Get all subCategories ;
    ApiProvider.prototype.getSubCategories = function (url, id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, subCategories, err_2;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    body.append('id', id);
                                    return [4 /*yield*/, this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    subCategories = _a.sent();
                                    this._subCategories.next(subCategories);
                                    resolve(this.subCategories);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_2 = _a.sent();
                                    console.log(err_2);
                                    reject(err_2);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    // Get all products ;
    ApiProvider.prototype.getProducts = function (url, id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, products, err_3;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    body.append('id', id);
                                    return [4 /*yield*/, this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    products = _a.sent();
                                    console.log("TP : ", products);
                                    this._products.next(products);
                                    resolve(products);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_3 = _a.sent();
                                    console.log(err_3);
                                    reject(err_3);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    //getMainPageProducts
    ApiProvider.prototype.getMainPageProducts = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, MainPageproducts, err_4, MainPageproducts;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    return [4 /*yield*/, this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    MainPageproducts = _a.sent();
                                    console.log("MainPageProducts ", MainPageproducts);
                                    this._MainPageproducts.next(MainPageproducts);
                                    resolve(MainPageproducts);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_4 = _a.sent();
                                    MainPageproducts = [];
                                    this._MainPageproducts.next(MainPageproducts);
                                    resolve(MainPageproducts);
                                    console.log(err_4, MainPageproducts);
                                    reject(err_4);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_6__config_config_app__["a" /* ConfigApp */],
            __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__["a" /* Restangular */]])
    ], ApiProvider);
    return ApiProvider;
}());

//
// com.tapper.ytravel
// SKU A unique ID for your app that is not visible on the App Store.
//     YTravel001
// Apple ID An automatically generated ID assigned to your app.
// 1362024077
//# sourceMappingURL=api.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AuthProvider = (function () {
    function AuthProvider(restangular, loadingCtrl, http) {
        this.restangular = restangular;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.ServerUrl = "http://limor.tapper.org.il/laravel/public/api/";
    }
    // Get all categories ;
    AuthProvider.prototype.LoginUser = function (url, params, push_id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, data, err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    body.append('mail', params.mail);
                                    body.append('password', params.password);
                                    body.append("push_id", push_id);
                                    return [4 /*yield*/, this.http.post(this.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    data = _a.sent();
                                    resolve(data);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_1 = _a.sent();
                                    console.log("Error : ", err_1);
                                    reject(err_1);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    // Get all categories ;
    AuthProvider.prototype.RegisterUser = function (url, params, push_id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, data, err_2;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    body = new FormData();
                                    body.append('name', params.name);
                                    body.append('mail', params.mail);
                                    body.append('address', params.address);
                                    body.append('phone', params.phone);
                                    body.append('newsletter', '1');
                                    body.append("push_id", push_id);
                                    body.append('mail', params.mail);
                                    body.append('password', params.password);
                                    return [4 /*yield*/, this.http.post(this.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    data = _a.sent();
                                    console.log("Reg : ", data);
                                    resolve(data);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_2 = _a.sent();
                                    console.log(err_2);
                                    reject(err_2);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    AuthProvider.prototype.SetUserPush = function (url, push_token, user_id) {
        if (push_token)
            localStorage.setItem("push_id", push_token);
        try {
            var body = new FormData();
            body.append("user_id", localStorage.getItem("userid"));
            body.append("push_id", localStorage.getItem("push_id"));
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
        }
    };
    AuthProvider.prototype.disconnectPush = function (url, user_id) {
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
        }
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["a" /* Restangular */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatmodalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_config_config_app__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChatmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatmodalPage = (function () {
    function ChatmodalPage(navCtrl, navParams, viewCtrl, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.Settings = Settings;
        this.imagePath = this.navParams.get('imagePath');
        this.phpHost = Settings.ServerImageHost;
    }
    ChatmodalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatmodalPage');
    };
    ChatmodalPage.prototype.ngOnInit = function () {
    };
    ChatmodalPage.prototype.closeImageModal = function () {
        this.viewCtrl.dismiss();
    };
    ChatmodalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-chatmodal',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\pages\chatmodal\chatmodal.html"*/`<!--\n  Generated template for the ChatModalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n      <button  ion-button icon-only clear navPop>\n        <ion-icon name="ios-close-circle" (click)="closeImageModal()"></ion-icon>\n      </button>\n    </div>\n\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <div style="width:100%;" align="center">\n    <zoom-area>\n      <img  src="{{phpHost+imagePath}}" style="width:100%;"/>\n    </zoom-area>\n  </div>\n\n\n\n</ion-content>\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\pages\chatmodal\chatmodal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_config_config_app__["a" /* ConfigApp */]])
    ], ChatmodalPage);
    return ChatmodalPage;
}());

//# sourceMappingURL=chatmodal.js.map

/***/ }),

/***/ 208:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 208;

/***/ }),

/***/ 252:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/basket/basket.module": [
		796,
		6
	],
	"../pages/chat/chat.module": [
		794,
		8
	],
	"../pages/chatmessages/chatmessages.module": [
		802,
		5
	],
	"../pages/chatmodal/chatmodal.module": [
		795,
		7
	],
	"../pages/home/home.module": [
		798,
		4
	],
	"../pages/login/login.module": [
		797,
		3
	],
	"../pages/product/product.module": [
		799,
		2
	],
	"../pages/products/products.module": [
		801,
		1
	],
	"../pages/register/register.module": [
		800,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 252;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__header_comp_header_comp__ = __webpack_require__(724);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__go_to_basket_popup_go_to_basket_popup__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__delete_popup_delete_popup__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__products_products__ = __webpack_require__(725);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__header_comp_header_comp__["a" /* HeaderCompComponent */],
                __WEBPACK_IMPORTED_MODULE_2__go_to_basket_popup_go_to_basket_popup__["a" /* GoToBasketPopupComponent */],
                __WEBPACK_IMPORTED_MODULE_4__delete_popup_delete_popup__["a" /* DeletePopupComponent */],
                __WEBPACK_IMPORTED_MODULE_5__products_products__["a" /* ProductsComponent */],
                __WEBPACK_IMPORTED_MODULE_5__products_products__["a" /* ProductsComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* IonicPageModule */].forChild([
                    __WEBPACK_IMPORTED_MODULE_1__header_comp_header_comp__["a" /* HeaderCompComponent */],
                    __WEBPACK_IMPORTED_MODULE_2__go_to_basket_popup_go_to_basket_popup__["a" /* GoToBasketPopupComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__delete_popup_delete_popup__["a" /* DeletePopupComponent */],
                ]),],
            exports: [__WEBPACK_IMPORTED_MODULE_1__header_comp_header_comp__["a" /* HeaderCompComponent */],
                __WEBPACK_IMPORTED_MODULE_2__go_to_basket_popup_go_to_basket_popup__["a" /* GoToBasketPopupComponent */],
                __WEBPACK_IMPORTED_MODULE_4__delete_popup_delete_popup__["a" /* DeletePopupComponent */],
                __WEBPACK_IMPORTED_MODULE_5__products_products__["a" /* ProductsComponent */],
                __WEBPACK_IMPORTED_MODULE_5__products_products__["a" /* ProductsComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_chat__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_config_config_app__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__chatmodal_chatmodal__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = (function () {
    function ChatPage(navCtrl, element, loadingCtrl, zone, navParams, ChatService, platform, Settings, events, actionSheet, camera, file, transfer, filePath, modalCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.element = element;
        this.loadingCtrl = loadingCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.platform = platform;
        this.Settings = Settings;
        this.events = events;
        this.actionSheet = actionSheet;
        this.camera = camera;
        this.file = file;
        this.transfer = transfer;
        this.filePath = filePath;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.recipent = 0;
        this.chatText = '';
        this.ChatArray = [];
        this.screenHeight = screen.height - 100 + 'px';
        this.rowNum = 1;
        this.lastImage = null;
        this.productArray = [];
        this.product_id = this.navParams.get('product_id');
        this.recipent = this.navParams.get('recipent');
        if (this.recipent == undefined)
            this.recipent = 0;
        this.userid = localStorage.getItem('userid');
        this.username = localStorage.getItem('name');
        this.is_Admin = localStorage.getItem('is_Admin');
        this.ChatService._ChatArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.ChatArray = val;
            });
        });
        this.phpHost = Settings.ServerImageHost;
        this.ServerUrl = Settings.ServerUrl;
        document.addEventListener('resume', function () {
            _this.getChatDetails();
        });
        events.subscribe('newchat', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.scrollBottom();
        });
        //this.addTextArea()
    }
    ChatPage.prototype.ngOnInit = function () {
        this.getChatDetails();
        this.getProductInfo();
        this.scrollBottom();
        this.innerHeight = (window.screen.height);
        var elm = document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px';
    };
    ChatPage.prototype.addTextArea = function () {
        this.TextAreaContent = document.getElementById('TextContent');
        console.log("TA : " + this.TextAreaContent);
        this.TextAreaContent.innerHTML = '<ion-textarea class="chatInput" id="chatInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>';
    };
    ChatPage.prototype.scrollBottom = function () {
        var _this = this;
        setTimeout(function () {
            //var objDiv = <HTMLElement>document.querySelector(".chatContent");
            //objDiv.scrollTop = objDiv.scrollHeight;
            var element = document.getElementById("chatContent");
            element.scrollIntoView(true);
            element.scrollTop = element.scrollHeight;
            _this.content.scrollToBottom(0);
        }, 300);
    };
    ChatPage.prototype.scrollToBottom = function () {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
        catch (err) { }
    };
    ChatPage.prototype.getLocalStorage11 = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.storage.get('userid').then(function (userid) {
                });
                return [2 /*return*/];
            });
        });
    };
    ChatPage.prototype.getProductInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.ChatService.getProductInfo('getProductInfo', this.product_id).then(function (data) {
                    console.log("Weights : ", data), _this.productArray = data;
                });
                return [2 /*return*/];
            });
        });
    };
    ChatPage.prototype.getChatDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var recipent_id;
            return __generator(this, function (_a) {
                if (this.recipent > 0)
                    recipent_id = this.recipent;
                else
                    recipent_id = this.userid;
                //alert (this.recipent)
                //alert (this.userid)
                this.ChatService.getChatDetails('getChatDetails', recipent_id, this.product_id).then(function (data) {
                    //this.loading.dismiss();
                    console.log("ChatDetails: ", data.product), _this.ChatArray = data.reverse(), _this.scrollBottom();
                });
                return [2 /*return*/];
            });
        });
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        this.scrollBottom();
        //console.log("TP : " , this.loading)
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        this.scrollBottom();
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    };
    ChatPage.prototype.addChatTitle = function (isImage) {
        var _this = this;
        var chatText1 = '';
        var chatType = '';
        if (isImage == 1) {
            chatText1 = this.uploadedImagePath;
            chatType = '3';
        }
        else {
            chatText1 = this.chatText;
            chatType = '1';
        }
        if (chatText1) {
            this.chatText = '';
            this.date = new Date();
            this.hours = this.date.getHours();
            this.minutes = this.date.getMinutes();
            this.seconds = this.date.getSeconds();
            if (this.hours < 10)
                this.hours = "0" + this.hours;
            if (this.minutes < 10)
                this.minutes = "0" + this.minutes;
            this.time = this.hours + ':' + this.minutes;
            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();
            if (this.dd < 10) {
                this.dd = '0' + this.dd;
            }
            if (this.mm < 10) {
                this.mm = '0' + this.mm;
            }
            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            //this.newdate = this.today + ' ' + this.time;
            this.Obj = {
                id: '',
                uid: localStorage.getItem('userid'),
                username: localStorage.getItem('name'),
                product_id: this.product_id,
                is_Admin: localStorage.getItem('is_Admin'),
                title: chatText1,
                date: this.today,
                time: this.time,
                type: chatType,
            };
            this.ChatService.pushToArray(this.Obj);
            this.ChatService.addTitle('addChatTitle', chatText1, this.today, this.time, localStorage.getItem('name'), '', chatType, this.product_id, this.is_Admin, this.recipent).then(function (data) {
                console.log("Weights : ", data), chatText1 = '', _this.scrollBottom(), _this.uploadedImagePath = '';
            });
            this.autoSize(40);
            //let txtArea = <HTMLElement>document.querySelector(".TextContent")
            //txtArea.innerHTML = '<ion-textarea class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)" #myInput></ion-textarea>';
            //txtArea.style.height = "auto";
        }
    };
    ChatPage.prototype.enlargeImage = function (newimage) {
        var modalObj = { imagePath: newimage };
        var ChatImageModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__chatmodal_chatmodal__["a" /* ChatmodalPage */], modalObj);
        ChatImageModal.present();
    };
    ChatPage.prototype.photoOptions = function () {
        var _this = this;
        var actionSheet = this.actionSheet.create({
            title: 'בחירת מקור התמונה',
            buttons: [
                {
                    text: 'גלריית תמונות',
                    icon: 'albums',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'מצלמה',
                    icon: 'camera',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ChatPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    ChatPage.prototype.uploadPhoto = function () {
        var _this = this;
        // Destination URL
        //var url = "http://tapper.org.il/647/laravel/public/api/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Loading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, this.ServerUrl + 'uploadImage', options).then(function (data) {
            console.log("Updata  : ", data.response);
            //this.serverImage = data.response;
            _this.loading.dismissAll();
            _this.uploadedImagePath = data.response;
            _this.addChatTitle(1);
        }, function (err) {
            _this.loading.dismissAll();
        });
    };
    ChatPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    _this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    _this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                });
            }
            else {
                console.log("f1");
                _this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : ", _this.currentName);
                _this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : ", _this.correctPath);
                _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                console.log("f4");
            }
        }, function (err) {
            //this.presentToast('Error while selecting image.');
        });
    };
    ChatPage.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    ChatPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2 : " + namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadPhoto();
        }, function (error) {
            //this.presentToast('Error while storing file.');
        });
    };
    ChatPage.prototype.autoSize = function (pixels) {
        if (pixels === void 0) { pixels = 0; }
        // let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
        var textArea = document.querySelector(".chatInput");
        console.log(textArea);
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        if (pixels === 0) {
            textArea.style.height = textArea.scrollHeight + 'px';
        }
        else {
            textArea.style.height = pixels + 'px';
        }
        return;
    };
    ChatPage.prototype.getHour = function (DateStr) {
        var Hour = DateStr.split(" ");
        var Hour1 = String(Hour[0]).split("/");
        return Hour1[0] + "/" + Hour1[1] + " | " + Hour[1];
    };
    ChatPage.prototype.cutDate = function (dt) {
        dt = dt.split(" ");
        return dt[0];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ChatPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('scrollMe'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], ChatPage.prototype, "myScrollContainer", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\pages\chat\chat.html"*/`<!--\n  Generated template for the TrainingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="white">\n    <button ion-button menuToggle [hidden]="true">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="mainTitle" (click)="enlargeImage(productArray.image)">\n      {{productArray.title}}\n      <img [src]="phpHost+productArray.image" *ngIf="productArray.image" style="width:40px; height:40px;">\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="ContentClass">\n\n  <!--\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  -->\n\n  <div class="chatContent" id="chatContent" #scrollMe [scrollTop]="scrollMe.scrollHeight" style="padding-bottom: 70px;">\n\n    <div class="chatMessage" id="chatMessage" *ngFor="let message of ChatArray">\n\n      <div class="chatMessageRight">\n        <!-- <img *ngIf="message.image" src="{{phpHost+message.image}}" class="imageclass"/> -->\n        <img *ngIf="!message.image"  src="images/avatar1.png" class="imageclass"/>\n      </div>\n      <div [ngClass]="{\'chatMessageLeft\' : message.is_Admin != 0, \'chatMessageLeftBlue\':message.is_Admin == 0}">\n        <div [ngClass]="{\'chatTitle\' : message.is_Admin != 0, \'chatTitleBlue\':message.is_Admin == 0}" >\n          <div [ngClass]="{\'chatTitleLeft\' : message.is_Admin != 0, \'chatTitleLeftBlue\':message.is_Admin ==  0}" >\n            <p>{{message.date}}</p>\n          </div>\n          <div [ngClass]="{\'chatTitleRight\' : message.is_Admin != 0, \'chatTitleRightBlue\':message.is_Admin == 0}" >\n            <p >{{message.username}}</p>\n          </div>\n        </div>\n        <p class="chatMessageLeftP" >{{message.title}}</p>\n      </div>\n\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer class="footerClass">\n  <div class="chatFooterDiv" align="center">\n    <div class="chatFooterDivRight">\n      <ion-item align="center" class="TextContent">\n        <!--<ion-textarea (input)="autoSize()" class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>-->\n        <ion-textarea (input)="autoSize();"  class="chatInput" rows="1"  autosize placeholder="הוסף הודעה" [(ngModel)]="chatText"></ion-textarea>\n      </ion-item>\n    </div>\n    <div class="chatFooterDivLeft" (click)="addChatTitle(0)">\n      <img src="images/addchat.png" class="imageclass"/>\n    </div>\n\n  </div>\n\n</ion-footer>`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_chat__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_8__providers_config_config_app__["a" /* ConfigApp */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the PopupsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PopupsProvider = (function () {
    function PopupsProvider(alertCtrl) {
        this.alertCtrl = alertCtrl;
        console.log('Hello PopupsProvider Provider');
    }
    PopupsProvider.prototype.presentAlert = function (Title, Message, PageRedirect, fun) {
        if (fun === void 0) { fun = null; }
        var alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                    text: 'סגור',
                    handler: function () { if (PageRedirect == 1) {
                        fun;
                    } }
                }],
            cssClass: 'alertRtl'
        });
        alert.present();
    };
    PopupsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], PopupsProvider);
    return PopupsProvider;
}());

//# sourceMappingURL=popups.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeletePopupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DeletePopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var DeletePopupComponent = (function () {
    function DeletePopupComponent(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    DeletePopupComponent.prototype.onSubmit = function (type) {
        this.viewCtrl.dismiss(type);
    };
    DeletePopupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'delete-popup',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\components\delete-popup\delete-popup.html"*/`<!-- Generated template for the GoToBasketPopupComponent component -->\n\n<div align="center" class="basketButtons">\n\n    <p class="question">Are you sure you want to delete this product ?</p>\n\n    <button ion-button class="basketButton" (click)="onSubmit(1)">DELETE</button>\n\n    <button ion-button class="basketButton" (click)="onSubmit(0)">CANCEL</button>\n\n</div>\n\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\components\delete-popup\delete-popup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], DeletePopupComponent);
    return DeletePopupComponent;
}());

//# sourceMappingURL=delete-popup.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoToBasketPopupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GoToBasketPopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var GoToBasketPopupComponent = (function () {
    function GoToBasketPopupComponent(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    GoToBasketPopupComponent.prototype.onSubmit = function (type) {
        this.viewCtrl.dismiss(type);
    };
    GoToBasketPopupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'go-to-basket-popup',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\components\go-to-basket-popup\go-to-basket-popup.html"*/`<!-- Generated template for the GoToBasketPopupComponent component -->\n\n<div align="center" class="basketButtons">\n\n    <button ion-button class="basketButton" (click)="onSubmit(1)">continue shopping</button>\n\n    <button ion-button class="basketButton" (click)="onSubmit(2)">go to basket</button>\n\n</div>\n\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\components\go-to-basket-popup\go-to-basket-popup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], GoToBasketPopupComponent);
    return GoToBasketPopupComponent;
}());

//# sourceMappingURL=go-to-basket-popup.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(418);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RestangularConfigFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(788);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_config_config_app__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_api_api_basket__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_chat__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_components_module__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ngx_restangular__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_auth_auth__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_popups_popups__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_firebase__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_chat_chat__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_chatmodal_chatmodal__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__directives_autosize_autosize__ = __webpack_require__(789);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_file__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_file_path__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ionic2_zoom_area__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_platform_browser_animations__ = __webpack_require__(792);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


























function RestangularConfigFactory(RestangularProvider) {
    var _this = this;
    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl("http://limor.tapper.org.il/laravel/public/api/");
    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
        if (data) {
            console.log(url, data, operation);
        }
        return data;
    });
    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(function (response, subject, responseHandler) { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            console.log('ErrorInterceptor', response);
            return [2 /*return*/];
        });
    }); });
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_17__pages_chat_chat__["a" /* ChatPage */], __WEBPACK_IMPORTED_MODULE_18__pages_chatmodal_chatmodal__["a" /* ChatmodalPage */], __WEBPACK_IMPORTED_MODULE_19__directives_autosize_autosize__["a" /* AutosizeDirective */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chatmodal/chatmodal.module#ChatmodalPageModule', name: 'ChatmodalPage', segment: 'chatmodal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/basket/basket.module#BasketPageModule', name: 'BasketPage', segment: 'basket', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product/product.module#ProductPageModule', name: 'ProductPage', segment: 'product', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chatmessages/chatmessages.module#ChatmessagesPageModule', name: 'ChatmessagesPage', segment: 'chatmessages', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_12__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_13_ngx_restangular__["b" /* RestangularModule */].forRoot([], RestangularConfigFactory),
                __WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_25__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_24_ionic2_zoom_area__["a" /* ZoomAreaModule */].forRoot(),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_17__pages_chat_chat__["a" /* ChatPage */], __WEBPACK_IMPORTED_MODULE_18__pages_chatmodal_chatmodal__["a" /* ChatmodalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_10__providers_api_api_basket__["a" /* BasketProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_api_chat__["a" /* ChatSevice */],
                __WEBPACK_IMPORTED_MODULE_9__providers_config_config_app__["a" /* ConfigApp */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_14__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_popups_popups__["a" /* PopupsProvider */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_19__directives_autosize_autosize__["a" /* AutosizeDirective */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_file_path__["a" /* FilePath */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConfigApp = (function () {
    function ConfigApp() {
        this.ServerImageHost = 'http://limor.tapper.org.il/laravel/storage/app/public/';
        this.ServerUrl = "http://limor.tapper.org.il/laravel/public/api/";
        this.UserId = '1';
        this.PushId = '';
        this.FullName = '';
        this.CourseId = '';
        this.push_id = '';
    }
    ;
    ConfigApp.prototype.SetUserPush = function (push_id) {
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    };
    ConfigApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ConfigApp);
    return ConfigApp;
}());

;
//# sourceMappingURL=config.app.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderCompComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api_basket__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HeaderCompComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeaderCompComponent = (function () {
    function HeaderCompComponent(viewCtrl, basketProvider, navCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.basketProvider = basketProvider;
        this.navCtrl = navCtrl;
        this.baskets = [];
        this.basketProvider.basket$.subscribe(function (basket) { return _this.baskets = basket; });
        console.log(this.baskets);
    }
    HeaderCompComponent.prototype.ionViewWillEnter = function () {
        this.viewCtrl.showBackButton(false);
    };
    HeaderCompComponent.prototype.gotoBasket = function () {
        console.log("bs");
        this.navCtrl.push('BasketPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], HeaderCompComponent.prototype, "name", void 0);
    HeaderCompComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'header-comp',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\components\header-comp\header-comp.html"*/`<!--<ion-navbar>-->\n\n    <!--&lt;!&ndash;<button ion-button menuToggle>&ndash;&gt;-->\n\n        <!--&lt;!&ndash;<ion-icon name="menu"></ion-icon>&ndash;&gt;-->\n\n    <!--&lt;!&ndash;</button>&ndash;&gt;-->\n\n    <!--&lt;!&ndash;<ion-title class="mainTitle">{{name}}</ion-title>&ndash;&gt;-->\n\n<!--</ion-navbar>-->\n\n\n\n<ion-navbar color="white">\n\n    <button ion-button menuToggle [hidden]="false">\n\n        <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title class="mainTitle">Limor</ion-title>\n\n    <button ion-button menuToggle end (click)="gotoBasket()">\n\n        <ion-icon class="iconHead" name="ios-basket-outline" >\n\n            <ion-badge id="notifications-badge"  color="danger" class="basketBadge" *ngIf="baskets"><span >{{baskets.length}}</span></ion-badge>\n\n        </ion-icon>\n\n    </button>\n\n</ion-navbar>\n\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\components\header-comp\header-comp.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api_basket__["a" /* BasketProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], HeaderCompComponent);
    return HeaderCompComponent;
}());

//# sourceMappingURL=header-comp.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


/**
 * Generated class for the ProductsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ProductsComponent = (function () {
    function ProductsComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ProductsComponent.prototype.gotoProductPage = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var product;
            return __generator(this, function (_a) {
                console.log("gotoProductPage : ", id);
                product = this.productsArray[id];
                this.navCtrl.push('ProductPage', { 'product': product });
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Array)
    ], ProductsComponent.prototype, "productsArray", void 0);
    ProductsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'products',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\components\products\products.html"*/`<!-- Generated template for the ProductsComponent component -->\n\n<div>\n\n    <div class="products">\n\n        <div class="productsList">\n\n            <div  class="productRowLeft" align="center"  (click)="gotoProductPage(i)" *ngFor="let product of productsArray let i = index" >\n\n                            <img src="{{productsArray[i].FullPath}}" class="mainImage" />\n\n                            <p class="productTitle">{{productsArray[i].title}}</p>\n\n                            <p class="productPrice">{{productsArray[i].high_price}} ש"ח</p>\n\n                            <div class="productButton">צפה בפרטים</div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\components\products\products.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], ProductsComponent);
    return ProductsComponent;
}());

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_api_basket__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_api_chat__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_config_config_app__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var MyApp = (function () {
    function MyApp(platform, statusBar, api, splashScreen, storage, basketProvider, alertCtrl, firebase, auth, events, ChatService, config) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.api = api;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.basketProvider = basketProvider;
        this.alertCtrl = alertCtrl;
        this.firebase = firebase;
        this.auth = auth;
        this.events = events;
        this.ChatService = ChatService;
        this.config = config;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: 'HomePage' },
            { title: 'LogOut', component: 'LogOut' }
        ];
        events.subscribe('userConnected', function (is_Admin) {
            if (is_Admin == "1") {
                var pageFound = 0;
                for (var i = 0; i < _this.pages.length; i++) {
                    if (_this.pages[i].component == 'ChatmessagesPage')
                        pageFound = 1;
                }
                if (pageFound == 0)
                    _this.pages.splice(1, 0, { title: 'Chat Messages', component: 'ChatmessagesPage' });
            }
            else {
                for (var i = 0; i < _this.pages.length; i++) {
                    if (_this.pages[i].component == 'ChatmessagesPage') {
                        _this.pages.splice(i, 1);
                    }
                }
            }
        });
    }
    MyApp.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var id, MainPageProducts, basket;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    //   this.platform.ready().then(() => {
                    //   // Okay, so the platform is ready and our plugins are available.
                    //   // Here you can do any higher level native things you might need.
                    //   this.statusBar.styleDefault();
                    //   this.splashScreen.hide();
                    // });
                    return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        //   this.platform.ready().then(() => {
                        //   // Okay, so the platform is ready and our plugins are available.
                        //   // Here you can do any higher level native things you might need.
                        //   this.statusBar.styleDefault();
                        //   this.splashScreen.hide();
                        // });
                        _a.sent();
                        this.statusBar.styleDefault();
                        if (this.platform.is('cordova')) {
                            if (this.platform.is('android')) {
                                this.initializeFireBaseAndroid();
                            }
                            if (this.platform.is('ios')) {
                                this.initializeFireBaseIos();
                            }
                        }
                        this.platform.registerBackButtonAction(function () {
                            if (_this.nav.length() == 1) {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Exit',
                                    message: 'Do you want to exit?',
                                    buttons: [
                                        {
                                            text: "OK", handler: function () {
                                                _this.platform.exitApp();
                                            }
                                        },
                                        { text: "Cancel", role: 'cancel' }
                                    ]
                                });
                                alert_1.present();
                            }
                            else
                                _this.nav.pop();
                        });
                        id = localStorage.getItem('userid');
                        if (!(id != "" && id != undefined && id != null)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.api.getCategories("GetCategories")];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.api.getMainPageProducts("getMainPageProducts")];
                    case 3:
                        MainPageProducts = _a.sent();
                        return [4 /*yield*/, this.storage.get('basket')];
                    case 4:
                        basket = _a.sent();
                        if (basket) {
                            basket = JSON.parse(basket);
                            this.basketProvider.addLocalStorageToBasket(basket);
                        }
                        return [4 /*yield*/, this.nav.setRoot('HomePage')];
                    case 5:
                        _a.sent(); // BasketPage
                        return [3 /*break*/, 8];
                    case 6:
                        console.log("MyId1 : ", id);
                        return [4 /*yield*/, this.nav.setRoot('RegisterPage')];
                    case 7:
                        _a.sent(); // LoginPage
                        _a.label = 8;
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.initializeFireBaseAndroid = function () {
        var _this = this;
        return this.firebase.getToken()
            .catch(function (error) {
            return console.error('Error getting token', error);
        })
            .then(function (token) {
            _this.firebase.subscribe('all').then(function (result) {
                if (result)
                    console.log("Subscribed to all");
                _this.subscribeToPushNotificationEvents();
            });
        });
    };
    MyApp.prototype.initializeFireBaseIos = function () {
        var _this = this;
        return this.firebase.grantPermission()
            .catch(function (error) { return console.error('Error getting permission', error); })
            .then(function () {
            _this.firebase.getToken()
                .catch(function (error) { return console.error('Error getting token', error); })
                .then(function (token) {
                console.log("The token is " + token);
                _this.firebase.subscribe('all').then(function (result) {
                    if (result)
                        console.log("Subscribed to all");
                    _this.subscribeToPushNotificationEvents();
                });
            });
        });
    };
    MyApp.prototype.sendPushtoserver = function (token) {
        this.config.push_id = token;
        this.auth.SetUserPush('SetUserPush', token, localStorage.getItem('userid'));
    };
    MyApp.prototype.saveToken = function (token) {
        // Send the token to the server
        // console.log('Sending token to the server...');
        this.sendPushtoserver(token);
        return Promise.resolve(true);
    };
    MyApp.prototype.subscribeToPushNotificationEvents = function () {
        var _this = this;
        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(function (token) {
            _this.sendPushtoserver(token);
            //this.saveToken(token);
            //this.loginService.sendToken('GetToken', token).then((data: any) => {
            //    console.log("UserDetails : ", data);
            //});
        }, function (error) {
            console.error('Error refreshing token', error);
        });
        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(function (notification) {
            // !notification.tap
            //     ? alert('The user was using the app when the notification arrived...')
            //     : alert('The app was closed when the notification arrived...');
            if (!notification.tap) {
                //alert(JSON.stringify(notification));
                _this.date = new Date();
                _this.hours = _this.date.getHours();
                _this.minutes = _this.date.getMinutes();
                _this.seconds = _this.date.getSeconds();
                if (_this.hours < 10)
                    _this.hours = "0" + _this.hours;
                if (_this.minutes < 10)
                    _this.minutes = "0" + _this.minutes;
                _this.time = _this.hours + ':' + _this.minutes;
                _this.today = new Date();
                _this.dd = _this.today.getDate();
                _this.mm = _this.today.getMonth() + 1; //January is 0!
                _this.yyyy = _this.today.getFullYear();
                if (_this.dd < 10) {
                    _this.dd = '0' + _this.dd;
                }
                if (_this.mm < 10) {
                    _this.mm = '0' + _this.mm;
                }
                _this.today = _this.dd + '/' + _this.mm + '/' + _this.yyyy;
                _this.newdate = _this.today + ' ' + _this.time;
                //alert("Notification " + notification.message );
                _this.Obj = {
                    title: notification.message,
                    date: _this.newdate,
                    //type: notification.type,
                    username: notification.fullname,
                    is_Admin: notification.is_Admin,
                };
                _this.ChatService.pushToArray(_this.Obj);
                _this.events.publish('newchat', "newchat");
                _this.events.publish('refreshAdminMessages', "refreshAdminMessages");
            }
            else {
                setTimeout(function () {
                    //alert(JSON.stringify(notification));
                    if (localStorage.getItem('userid')) {
                        if (localStorage.getItem('is_Admin') == "0") {
                            _this.nav.push('ChatPage', { product_id: notification.product_id });
                        }
                        else {
                            _this.nav.push('ChatmessagesPage');
                        }
                    }
                }, 3000);
            }
        }, function (error) {
            console.error('Error getting the notification', error);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component != 'LogOut')
            this.nav.push(page.component);
        else {
            this.auth.disconnectPush("disconnectPush", localStorage.getItem('userid'));
            this.storage.clear();
            localStorage.clear();
            this.nav.setRoot('LoginPage');
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\app\app.html"*/`<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__providers_api_api_basket__["a" /* BasketProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_9__providers_api_chat__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_10__providers_config_config_app__["a" /* ConfigApp */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutosizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutosizeDirective = (function () {
    function AutosizeDirective(element) {
        var _this = this;
        this.element = element;
        this.onInput = function (textArea) {
            _this.adjust();
        };
        this.adjust = function () {
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                ta.style.overflow = "hidden";
                ta.style.height = "auto";
                ta.style.height = ta.scrollHeight + "px";
            }
        };
    }
    AutosizeDirective.prototype.ngOnInit = function () {
        var _this = this;
        var waitThenAdjust = function (trial) {
            if (trial > 10) {
                // Give up.
                return;
            }
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                _this.adjust();
            }
            else {
                setTimeout(function () {
                    waitThenAdjust(trial + 1);
                }, 0);
            }
        };
        // Wait for the textarea to properly exist in the DOM, then adjust it.
        waitThenAdjust(1);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* HostListener */])("input", ["$event.target"]),
        __metadata("design:type", Object)
    ], AutosizeDirective.prototype, "onInput", void 0);
    AutosizeDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: "ion-textarea[autosize]" // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], AutosizeDirective);
    return AutosizeDirective;
}());

//# sourceMappingURL=autosize.js.map

/***/ })

},[413]);
//# sourceMappingURL=main.js.map
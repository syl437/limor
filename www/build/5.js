webpackJsonp([5],{

/***/ 802:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatmessagesPageModule", function() { return ChatmessagesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chatmessages__ = __webpack_require__(809);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatmessagesPageModule = (function () {
    function ChatmessagesPageModule() {
    }
    ChatmessagesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chatmessages__["a" /* ChatmessagesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chatmessages__["a" /* ChatmessagesPage */]),
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ]
        })
    ], ChatmessagesPageModule);
    return ChatmessagesPageModule;
}());

//# sourceMappingURL=chatmessages.module.js.map

/***/ }),

/***/ 809:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatmessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_config_config_app__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_chat__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChatmessagesPage = (function () {
    function ChatmessagesPage(navCtrl, navParams, events, ChatService, zone, config) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.ChatService = ChatService;
        this.zone = zone;
        this.config = config;
        this.MessagesArray = [];
        this._MessagesArray = new __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__["Subject"]();
        this.MessagesArray$ = this._MessagesArray.asObservable();
        this.ServerImageHost = this.config.ServerImageHost;
        document.addEventListener('resume', function () {
            _this.getAdminMessages();
        });
        events.subscribe('refreshAdminMessages', function (user, time) {
            _this.getAdminMessages();
        });
        this._MessagesArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.MessagesArray = val;
            });
        });
    }
    ChatmessagesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatmessagesPage');
    };
    ChatmessagesPage.prototype.getAdminMessages = function () {
        var _this = this;
        this.MessagesArray = [];
        this.ChatService.getAdminMessages('getAdminMessages').then(function (data) {
            _this.MessagesArray = data;
            _this._MessagesArray.next(_this.MessagesArray);
            console.log("getAdminMessages: ", data);
        });
    };
    ChatmessagesPage.prototype.ngOnInit = function () {
    };
    ChatmessagesPage.prototype.ionViewWillEnter = function () {
        this.getAdminMessages();
    };
    ChatmessagesPage.prototype.goToChatPage = function (recipent, product_id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* ChatPage */], {
            recipent: recipent,
            product_id: product_id
        });
    };
    ChatmessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-chatmessages',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\limor\src\pages\chatmessages\chatmessages.html"*/`<!--\n  Generated template for the ChatmessagesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>LIMOR</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list>\n    <ion-item *ngFor="let message of MessagesArray" (click)="goToChatPage(message.user_id,message.product_id)">\n      <ion-thumbnail item-start>\n        <img *ngIf="!message.product_image" src="images/avatar1.png">\n        <img *ngIf="message.product_image" [src]="ServerImageHost+message.product_image">\n      </ion-thumbnail>\n      <h2>{{message.username}}</h2>\n      <p>{{message.product_details}}</p>\n      <p>{{message.chat_date}}</p>\n      <p>{{message.last_chat}}</p>\n      <button ion-button clear item-end>View</button>\n      <ion-badge id="notifications-badge"  color="danger" class="basketBadge" *ngIf="message.is_read == 0"><span >1</span></ion-badge>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n`/*ion-inline-end:"C:\Users\USER\Desktop\github\limor\src\pages\chatmessages\chatmessages.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_3__providers_api_chat__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */], __WEBPACK_IMPORTED_MODULE_2__providers_config_config_app__["a" /* ConfigApp */]])
    ], ChatmessagesPage);
    return ChatmessagesPage;
}());

//# sourceMappingURL=chatmessages.js.map

/***/ })

});
//# sourceMappingURL=5.js.map